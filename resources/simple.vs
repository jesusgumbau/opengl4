#version 410
in vec3 position;
in vec4 color;
out vec4 color0;
uniform mat4 trans;
void main(){
  gl_Position = trans*vec4(position, 1);
  color0 = color;
}