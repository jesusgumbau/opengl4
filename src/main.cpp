#include <iostream>
#include <math.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "matrix4.h"
#include "shader.h"
#include "mesh.h"

int main()
{
  if (!glfwInit()){
    std::cout << "GLFW init failed!\n";
  }

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, true);

  GLFWwindow *window = glfwCreateWindow(512,512,"Hello", NULL, NULL);
  glfwMakeContextCurrent(window);

  if (glewInit() != GLEW_OK){
    std::cout << "GLEW init failed!\n";
  }

  const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
  const GLubyte* version = glGetString(GL_VERSION); // version as a string
  printf("Renderer: %s\n", renderer);
  printf("OpenGL version supported %s\n", version);

  Matrix4 matrix;

  Shader shader("resources/simple.vs", "resources/simple.ps");
  Mesh mesh;

  GLint POSITION_ATTRIB_LOC = shader.GetAttribLocation("position");
  GLint COLOR_ATTRIB_LOC = shader.GetAttribLocation("color");
  GLint uniTrans = shader.GetUniformLocation("trans");

  enum {POSITION, COLOR, NUM_BUFFERS};
  unsigned int vao, vbo[NUM_BUFFERS];
  unsigned int indexBuffer;

  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  glGenBuffers(NUM_BUFFERS, vbo);
  glGenBuffers(1, &indexBuffer);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * mesh.GetNumIndices(), mesh.GetIndices(), GL_STATIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, vbo[POSITION]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(Vector3) * mesh.GetNumVertices(), mesh.GetPositions(), GL_STATIC_DRAW);
  glEnableVertexAttribArray(POSITION_ATTRIB_LOC);
  glVertexAttribPointer(POSITION_ATTRIB_LOC, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

  glBindBuffer(GL_ARRAY_BUFFER, vbo[COLOR]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(Vector4) * mesh.GetNumVertices(), mesh.GetColors(), GL_STATIC_DRAW);
  glEnableVertexAttribArray(COLOR_ATTRIB_LOC);
  glVertexAttribPointer(COLOR_ATTRIB_LOC, 4, GL_FLOAT, GL_FALSE, 0, (void*)0);

  shader.Use();
  
  Matrix4 rotMatrixX = Matrix4::RotationX(0.005f);  
  Matrix4 rotMatrixY = Matrix4::RotationY(0.005f);  
  Matrix4 rotMatrixZ = Matrix4::RotationZ(0.005f);  
  glClearColor(0.1,1,0.5,1);
  glEnable(GL_DEPTH_TEST);
  while (!glfwWindowShouldClose(window)){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    shader.Use();
    glBindVertexArray(vao);

    matrix = matrix*rotMatrixX*rotMatrixY*rotMatrixZ;
    glUniformMatrix4fv (uniTrans, 1, GL_FALSE, matrix);

    glDrawElements(GL_TRIANGLES, mesh.GetNumIndices(), GL_UNSIGNED_INT, (void*)0);

    glfwPollEvents();
    glfwSwapBuffers(window);
  }
}
