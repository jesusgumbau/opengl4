#ifndef __VECTOR4_h__
#define __VECTOR4_h__

class Vector4
{
public:
  float x=0, y=0, z=0, w=0;
  Vector4();
  Vector4(float x, float y, float z, float w);
  float& operator[](int index);
  float operator[](int index) const;
};

#endif
