#include "vector3.h"
#include "vector4.h"

class Mesh
{
public:
  Mesh();
  ~Mesh();
  const Vector3* GetPositions() const { return positions; }
  const Vector4* GetColors() const { return colors; }
  const unsigned int* GetIndices() const { return indices; }
  unsigned int GetNumVertices() const { return numVertices; }
  unsigned int GetNumIndices() const { return numIndices; }
  
private:
  Vector3* positions;
  Vector4* colors;
  unsigned int* indices;
  unsigned int numVertices;
  unsigned int numIndices;
};
