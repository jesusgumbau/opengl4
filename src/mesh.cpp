#include "mesh.h"
#include <string.h>

Mesh::Mesh()
{
  numVertices = 8;
  numIndices = 36;
  positions = new Vector3[numVertices];
  positions[0] = Vector3(-0.5, -0.5, -0.5);
  positions[1] = Vector3(-0.5, -0.5, 0.5);
  positions[2] = Vector3(-0.5, 0.5, -0.5);
  positions[3] = Vector3(-0.5, 0.5, 0.5);
  positions[4] = Vector3( 0.5, -0.5, -0.5);
  positions[5] = Vector3( 0.5, -0.5, 0.5);
  positions[6] = Vector3( 0.5, 0.5, -0.5);
  positions[7] = Vector3( 0.5, 0.5, 0.5);

  colors = new Vector4[numVertices];
  colors[0] = Vector4(1,0,0,1);
  colors[1] = Vector4(0,1,0,1);
  colors[2] = Vector4(0,0,1,1);
  colors[3] = Vector4(1,1,1,1);
  colors[4] = Vector4(1,0,0,1);
  colors[5] = Vector4(0,1,0,1);
  colors[6] = Vector4(0,0,0,1);
  colors[7] = Vector4(1,1,1,1);

  const unsigned int srcIndices[] = {
    0,6,4,
    0,2,6,
    0,3,2,
    0,1,3,
    2,7,6,
    2,3,7,
    4,6,7,
    4,7,5,
    0,4,5,
    0,5,1,
    1,5,7,
    1,7,3
   };

   indices = new unsigned int[numIndices];
   memcpy(indices, srcIndices, numIndices * sizeof(unsigned int));
}

Mesh::~Mesh()
{
  delete[] positions;
  delete[] colors;
  delete[] indices;
}
