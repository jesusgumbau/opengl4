#include <math.h>
#include <cstring>
#include "matrix4.h"

Matrix4::Matrix4(){
  memset(array, 0, 16*sizeof(float));
  array[0].x=1;
  array[1].y=1;
  array[2].z=1;
  array[3].w=1;
}

Matrix4 Matrix4::Identity(){
  return Matrix4();
}

Matrix4 Matrix4::Translation(Vector4 position){
  Matrix4 trans;
  trans.array[3].x = position.x;
  trans.array[3].y = position.y;
  trans.array[3].z = position.z;
  return trans;
}

Matrix4 Matrix4::RotationX(float angle){
  Matrix4 rot;
  rot.array[1].y=cosf(angle);
  rot.array[1].z=sinf(angle);
  rot.array[2].y=-sinf(angle);
  rot.array[2].z=cosf(angle);
  return rot;
}

Matrix4 Matrix4::RotationY(float angle){
  Matrix4 rot;
  rot.array[0].x=cosf(angle);
  rot.array[0].z=-sinf(angle);
  rot.array[2].x=sinf(angle);
  rot.array[2].z=cosf(angle);
  return rot;
}

Matrix4 Matrix4::RotationZ(float angle){
  Matrix4 rot;
  rot.array[0].x=cosf(angle);
  rot.array[0].y=sinf(angle);
  rot.array[1].x=-sinf(angle);
  rot.array[1].y=cosf(angle);
  return rot;
}

Matrix4::operator const float*(){
  return (const float*)array;
}

Matrix4 Matrix4::operator*(const Matrix4& other){
  Matrix4 result;
  memset(result.array, 0, 16*sizeof(float));
  for(int i=0; i<4; i++)
	for(int j=0; j<4; j++)
       for(int k=0; k<4; k++)
         result.array[i][j] += array[k][j] * other.array[i][k];
  return result;
}

Matrix4& Matrix4::operator=(const Matrix4& other){
  memcpy(array, other.array, 16*sizeof(float));
  return *this;
}
