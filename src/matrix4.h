#include "vector4.h"

class Matrix4 {
  Vector4 array[4];
public:
  Matrix4();
  static Matrix4 Identity();
  static Matrix4 Translation(Vector4 position);
  static Matrix4 RotationX(float angle);
  static Matrix4 RotationY(float angle);
  static Matrix4 RotationZ(float angle);
  operator const float*();
  Matrix4& operator= (const Matrix4& other);
  Matrix4 operator* (const Matrix4& other);
};
