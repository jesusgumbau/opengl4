#include <iostream>
#include <fstream>
#include <string>
#include <GL/glew.h>
#include "shader.h"

using namespace std;

Shader::Shader(const char* vsFileName, const char* psFileName){
  shaderProgram = glCreateProgram();
  vs = glCreateShader(GL_VERTEX_SHADER);
  ps = glCreateShader(GL_FRAGMENT_SHADER);
  LoadShaderFile(vsFileName, vs);
  LoadShaderFile(psFileName, ps);

  glAttachShader(shaderProgram, vs);
  glAttachShader(shaderProgram, ps);

  glLinkProgram(shaderProgram);
}

void Shader::LoadShaderFile(const char* fileName, unsigned int& shaderId){
  string fileContents;
  string line;
  ifstream myfile(fileName);
  if (myfile.is_open())
  {
    while (getline(myfile,line))
      fileContents += line + "\n";
    myfile.close();

    const char* sourceCode = fileContents.c_str();
    glShaderSource(shaderId, 1, &sourceCode, NULL);
    glCompileShader(shaderId);
  }
  else
    throw runtime_error(string("Unable to open file: ") + fileName);
}

unsigned int Shader::GetAttribLocation(const char* name){
  return glGetAttribLocation(shaderProgram, name);
}

unsigned int Shader::GetUniformLocation(const char* name){
  return glGetUniformLocation(shaderProgram, name);
}

void Shader::Use()
{
  glUseProgram(shaderProgram);
  glValidateProgram(shaderProgram);

  int testVal=0;
  glGetProgramiv(shaderProgram, GL_VALIDATE_STATUS, &testVal);
  if(testVal == GL_FALSE)
    {
      char infolog[1024];
      glGetProgramInfoLog(shaderProgram, 1024, NULL, infolog);
      throw runtime_error(infolog);
    }
}
  
