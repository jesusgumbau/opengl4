#include "vector4.h"

Vector4::Vector4(){
  this->x = 0;
  this->y = 0;
  this->z = 0;
  this->w = 0;
}

Vector4::Vector4(float x, float y, float z, float w){
  this->x = x;
  this->y = y;
  this->z = z;
  this->w = w;
}

float& Vector4::operator[](int index){
  if(index == 0) return this->x;
  if(index == 1) return this->y;
  if(index == 2) return this->z;
  return this->w;
}

float Vector4::operator[](int index) const {
	if(index == 0) return this->x;
	if(index == 1) return this->y;
	if(index == 2) return this->z;
	return this->w;
}
