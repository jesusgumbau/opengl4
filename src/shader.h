class Shader
{
public:
  Shader(const char* vsFileName, const char* psFileName);
  unsigned int GetAttribLocation(const char* name);
  unsigned int GetUniformLocation(const char* name);
  void Use();
  
private:
  void LoadShaderFile(const char* fileName, unsigned int& shaderId);

  unsigned int shaderProgram;
  unsigned int vs, ps;
};
