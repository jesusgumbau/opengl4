struct Vector3 {
public:
  float x=0, y=0, z=0;
  Vector3();
  Vector3(float x, float y, float z);
  float& operator[](int index);
  float operator[](int index) const;
};
