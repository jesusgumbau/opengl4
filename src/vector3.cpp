#include "vector3.h"

Vector3::Vector3(){
  this->x = 0;
  this->y = 0;
  this->z = 0;
}

Vector3::Vector3(float x, float y, float z){
  this->x = x;
  this->y = y;
  this->z = z;
}

float& Vector3::operator[](int index){
  if(index == 0) return this->x;
  if(index == 1) return this->y;
  return this->z;
}

float Vector3::operator[](int index) const {
	if(index == 0) return this->x;
	if(index == 1) return this->y;
	return this->z;
}
